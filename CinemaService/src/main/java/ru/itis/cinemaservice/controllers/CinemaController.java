package ru.itis.cinemaservice.controllers;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.cinemaservice.controllers.api.CinemaApi;
import ru.itis.cinemaservice.dto.CinemaDto;
import ru.itis.cinemaservice.services.CinemaService;

import java.util.List;

@RestController
@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class CinemaController implements CinemaApi {

    CinemaService cinemaService;

    @Override
    public ResponseEntity<List<CinemaDto>> getCinemas(String cityName) {
        List<CinemaDto> cinemas = cinemaService.getCityCinemas(cityName);

        if (cinemas == null || cinemas.isEmpty()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }

        return ResponseEntity.ok(cinemas);
    }
}

