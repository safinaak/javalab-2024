package ru.itis.cinemaservice.services.impl;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;
import ru.itis.cinemaservice.dto.CinemaDto;
import ru.itis.cinemaservice.repositories.CinemaRepository;
import ru.itis.cinemaservice.services.CinemaService;

import java.util.List;

@Service
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@RequiredArgsConstructor
public class CinemaServiceImpl implements CinemaService {

    CinemaRepository cinemasRepository;

    @Override
    public List<CinemaDto> getCityCinemas(String cityName) {
        return CinemaDto.from(cinemasRepository.getAllByCityName(cityName));
    }
}

