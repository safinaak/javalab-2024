package ru.itis.cinemaservice.exceptions;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import ru.itis.cinemaservice.dto.ExceptionDto;

@ControllerAdvice
public class RestExceptionHandler {

    @ExceptionHandler(RestException.class)
    public ResponseEntity<ExceptionDto> handleRestException(RestException e) {
        return ResponseEntity
                .status(e.getStatus())
                .body(ExceptionDto.builder()
                        .message(e.getMessage())
                        .build());
    }
}

